
**Question 1:**

Afficher le nom des Chercheurs faisant partie de l'Equipe immunologie.

**Answer:**

```sql
select Chercheur.nom from Chercheur, Equipe 
where (
    Chercheur.idEquipe = Equipe.id and 
    Equipe.nom = 'IMMUNOLOGIE'
);
```

ou

```sql
select C.nom from Chercheur as C, Equipe as E 
where C.idEquipe = E.id and E.nom = 'IMMUNOLOGIE';
```

---

**Question 2:**

Creer la table Article.

**Answer:**

```sql
create table Article(
    ref varchar(255) not null, 
    titre varchar(255), 
    annee int, 
    primary key(ref)
);
```

---

**Question 3:**

Creer table Equipe.

**Answer:**

```sql
create table Equipe(
    id int not null, 
    nom varchar(255), 
    idDirecteur int, 
    primary key(id), 
    foreign key (idDirecteur) References Chercheur(id)
);

```

--- 

**Question 4:**

Afficher les Titres des articles qui ont été publiés à partir de 2015 et par un chercheur du service d'Immunologie

**Answer:**

```sql
select titre from Article, Redige, Chercheur, Equipe
where (
    Article.annee >= 2015 and 
    Article.ref = Redige.refArticle and
    Redige.idChercheur = Chercheur.id and
    Chercheur.idEquipe = Equipe.id and
    Equipe.nom = "IMMUNOLOGIE"
    );
```

---

**Question 5:**

Afficher les chercheurs qui n'ont pas publié d'articles

**Answer:**

```sql
select nom from Chercheur
where not exists (
    select '' from Redige where Redige.idChercheur = Chercheur.id
);
```