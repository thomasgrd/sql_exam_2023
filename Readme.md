
# Preparation for the exam

To prepare the exam, several new SQL requests are presented here:
[Preparation](Preparation_exam.md)

>I have already made these requests yesterday and I will add the correction when you will have realized the SQL requests.
These new requests are also based on the same tables used for the exam [(See the Tables)](DB_for_testing.md).

# Exam 

* [Exo 1](Exo_1.md)

# DB for testing

A filled database has been created in order to test the sql requets of the Exam.

An overview of the different tables is here:
[Overview of Tables](DB_for_testing.md)

