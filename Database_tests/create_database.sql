-- If the database "exam_1" already exists, delete it to start from a clean database
drop database exam_1;

-- Create our database called "exam_1"
create database exam_1;

-- Enter on our database
use exam_1;

-- Create the following Tables
create table Article(ref varchar(255) not null, titre varchar(255), annee int, primary key(ref));
create table Redige(idChercheur int not null, refArticle varchar(255) not null,primary key(idChercheur,refArticle));
create table Equipe(id int not null, nom varchar(255), idDirecteur int, primary key(id));
create table Chercheur(id int not null, nom varchar(255), age int, idEquipe int, primary key(id));

'''
No foreigns keys defined (not necessary for the tests):
We had some problems with the foreign keys, 
when we add foreign keys when creating our tables, after that
we are not able to add some values on the tables. 
'''
