
# Basic SQL syntax

Afficher le nom des Chercheurs agés de 30 ans ou 40 ans.
```sql
select nom from Chercheur where age = 30 or age = 40; 
```

Afficher le nom des Chercheurs n'étant pas agés de 25 ans.

(_Hint: in sql `<>` means "different"._)
```sql
select nom from Chercheurs where age <> 25 ans;
```

Afficher le titre des articles publiés entre les années 2010 et 2020.
```sql
select * from Article where annee >= 2010 and annee <= 2020;
```

# SQL requests using a path over 4 tables

Afficher le titre des articles publiés par l'Equipe d'Immunologie.
```sql
select Article.titre from Article, Redige, Equipe, Chercheur
where (
    Article.ref = Redige.refArticle and
    Redige.idChercheur = Chercheur.id and
    Chercheur.idEquipe = Equipe.id and
    Equipe.nom = "IMMUNOLOGIE"
);
```

# SQL request using a path over 3 tables

Afficher le titre des Articles publiés par "Yagami"
```sql
select titre from Article, Redige, Chercheur
where (
    Article.ref = Redige.refArticle and
    Redige.idChercheur = Chercheur.id and
    Chercheur.age >= 25
);
```

# SQL requests using a path over 2 tables

Afficher le nom des chercheurs agé de moins de 40 ans faisant partie de l'Equipe Cancerologie.
```sql
select Chercheur.nom from Chercheur, Equipe
where (
    Chercheur.idEquipe = Equipe.id and 
    Equipe.nom = "CANCEROLOGIE" and
    Chercheur.age < 40
);
```

Nom des chercheurs qui sont également Directeur
```sql
select Chercheur.nom from Chercheur, Equipe
where (
    Chercheur.id = Equipe.idDirecteur
);
```