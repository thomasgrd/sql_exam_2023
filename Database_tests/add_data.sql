-- The "exam_1" database will be used
use  exam_1;

-- Add 3 teams on our "Equipe" table
insert into Equipe(id, nom, iddirecteur)
values
(1, "IMMUNOLOGIE", 5),
(2, "CANCEROLOGIE", 3),
(3, "PSYCATHRIE", 1);

-- Add 5 persons on our "Chercheur" table
insert into Chercheur(id, nom, age, idEquipe) 
values
(1, "GRONDIN", 22, 3),
(2, "PAYET", 32, 3),
(3,"HOAREAU" ,25 , 2),
(4, "YAGAMI", 40, 2),
(5, "PATATE", 19, 1),
(6, "ELDRIC", 30, 2);

-- Add 9 published Articles
insert into Article(ref, titre, annee)
values
("REF20231031-cdx", "les virus chez les souris", 2023),
("REF20161231-cfs", "les virus chez les francais", 2016),
("REF19981031-cgd", "le burn out chez les jeunes", 1998),
("REF20201101-cid", "le burn out chez les vieux", 2020),
("REF20231021-cel", "violence familiale", 2023),
("REF20071031-spd", "cancer estomac", 2007),
("REF20110430-ozf", "cancer reins", 2011),
("REF20221031-ipl", "cauchemars chez les jeunes", 2022),
("REF20121031-pqv", "les virus chez les poissons", 2012);

-- Add 9 values: Articles linked to their Chercheur
insert into Redige(idchercheur, refarticle)
values
(1, "REF19981031-cgd"),
(1, "REF20201101-cid"),
(1, "REF20221031-ipl"),
(2, "REF20231021-cel"),
(3, "REF20071031-spd"),
(4, "REF20110430-ozf"),
(5, "REF20231031-cdx"),
(5, "REF20161231-cfs"),
(5, "REF20121031-pqv");
