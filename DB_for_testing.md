# A filled database for our tests

## An overview the different tables

```sql
MariaDB [exam_1]> select * from Chercheur;
+----+---------+------+----------+
| id | nom     | age  | idEquipe |
+----+---------+------+----------+
|  1 | GRONDIN |   22 |        3 |
|  2 | PAYET   |   32 |        3 |
|  3 | HOAREAU |   25 |        2 |
|  4 | YAGAMI  |   40 |        2 |
|  5 | PATATE  |   19 |        1 |
|  6 | ELDRIC  |   30 |        2 |
+----+---------+------+----------+
6 rows in set (0.000 sec)

MariaDB [exam_1]> select * from Equipe;
+----+--------------+-------------+
| id | nom          | idDirecteur |
+----+--------------+-------------+
|  1 | IMMUNOLOGIE  |           5 |
|  2 | CANCEROLOGIE |           3 |
|  3 | PSYCATHRIE   |           1 |
+----+--------------+-------------+
3 rows in set (0.000 sec)

MariaDB [exam_1]> select * from Redige;
+-------------+-----------------+
| idChercheur | refArticle      |
+-------------+-----------------+
|           1 | REF19981031-cgd |
|           1 | REF20201101-cid |
|           1 | REF20221031-ipl |
|           2 | REF20231021-cel |
|           3 | REF20071031-spd |
|           4 | REF20110430-ozf |
|           5 | REF20121031-pqv |
|           5 | REF20161231-cfs |
|           5 | REF20231031-cdx |
+-------------+-----------------+
9 rows in set (0.000 sec)

MariaDB [exam_1]> select * from Article;
+-----------------+-----------------------------+-------+
| ref             | titre                       | annee |
+-----------------+-----------------------------+-------+
| REF19981031-cgd | le burn out chez les jeunes |  1998 |
| REF20071031-spd | cancer estomac              |  2007 |
| REF20110430-ozf | cancer reins                |  2011 |
| REF20121031-pqv | les virus chez les poissons |  2012 |
| REF20161231-cfs | les virus chez les francais |  2016 |
| REF20201101-cid | le burn out chez les vieux  |  2020 |
| REF20221031-ipl | cauchemars chez les jeunes  |  2022 |
| REF20231021-cel | violence familiale          |  2023 |
| REF20231031-cdx | les virus chez les souris   |  2023 |
+-----------------+-----------------------------+-------+
9 rows in set (0.000 sec)
```

## Create a DB for our tests

How to use the database of test on a linux machine.

* Start a "mariadb" database
```sh
sudo systemctl start mariadb
```

* Run the SQL scripts:
```sh
sudo mariadb < create_database.sql
sudo mariadb < add_data.sql
```

Now, our database "exam_1" has been created and filled with specific data. We can use this database to test our SQL requests.

* Test a SQL request

Write your SQL request in a sql file (for example "my_tests.sql") and execute it on the following way.
```sh
sudo mariadb < my_tests.sql
```