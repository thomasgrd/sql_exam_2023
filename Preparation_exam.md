
# Basic SQL syntax

Afficher le nom des Chercheurs agés de 30 ans ou 40 ans.
```sql

```

Afficher le nom des Chercheurs n'étant pas agés de 25 ans.

(_Hint: in sql `<>` means "different"._)
```sql

```

Afficher le titre des articles publiés entre les années 2010 et 2020.
```sql

```

# SQL requests using a path over 4 tables

Afficher le titre des articles publiés par l'Equipe d'Immunologie.
```sql

```

# SQL request using a path over 3 tables

Afficher le titre des Articles publiés par "Yagami"
```sql

```

# SQL requests using a path over 2 tables

Afficher le nom des chercheurs agé de moins de 40 ans faisant partie de l'Equipe Cancerologie.
```sql

```

Nom des chercheurs qui sont également Directeur
```sql

```